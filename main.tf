provider "aws" {
    access_key = var.access_key
    secret_key = var.secrect_key
    region     = var.region
}

resource "aws_instance" "teste_diego" {
    ami = "ami-00068cd7555f543d5"
    instance_type = "t2.micro"
}